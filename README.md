# Microtick Examples

For all the subdirectories in this repository, it is assumed you have a running local
testnet. Running a local testnet is simple, just follow the steps below:

## Step 1 - Set up a local network

Download the latest [binary release](https://microtick.com/releases) and drop the binaries in your
path (/usr/bin in the example below):

```
$ tar xf microtick-v1.0.0-linux-x86_64.tar.gz
$ sudo cp mtd mtcli /usr/bin
```

## Step 2 - Choose an empty directory for your testnet

The directory can be anywhere, we're using $HOME/mylocaltestnet below:

```
$ export MTROOT=$HOME/mylocaltestnet
$ mkdir -p $MTROOT
$ mtd init localnode --chain-id mylocalchain
```

## Step 3 - Add a Bitcoin market with a couple durations

Edit the newly created file $MTROOT/mtd/config/genesis.json and add some markets and durations:

- Set app_state.microtick.markets => [{name:"XBTUSD",description:"Crypto - Bitcoin"}]
- Set app_state.microtick.durations => [{name:"5minute",seconds:300},{name:"15minute",seconds:900},{name:"1hour",seconds:3600}]

If you have the tool 'jq' and 'sponge' installed (sudo apt-get install jq moreutils), you can issue the following commmands:

```
$ TRANSFORMS='.app_state.microtick.markets=[{name:"XBTUSD",description:"Crypto - Bitcoin"}]'
$ TRANSFORMS+='|.app_state.microtick.durations=[{name:"5minute",seconds:300},{name:"15minute",seconds:900},{name:"1hour",seconds:3600}]'
$ jq "$TRANSFORMS" $MTROOT/mtd/config/genesis.json | sponge $MTROOT/mtd/config/genesis.json
```

## Step 4 - Create some local accounts and run the node

Now, generate a default validator and a default bank account, and run the node:

```
$ mtcli config chain-id mylocalchain
$ mtcli config keyring-backend file
$ mtcli keys add validator
$ mtcli keys add bank
$ mtd add-genesis-account validator 1000000000000stake --keyring-backend file
$ mtd add-genesis-account bank 1000000000000udai --keyring-backend file
$ mtd gentx --name validator --amount 1000000000000stake --keyring-backend file
$ mtd collect-gentxs
$ mtd start
```

## Step 5 - Run the mtcli REST server

At this point your node should be running. Switch to another window or terminal and run a 
Tendermint rest-server:

```
$ export MTROOT=$HOME/mylocaltestnet
$ mtcli rest-server
```

## Step 6 - Run the Microtick API server

Now you can run a local instance of the Microtick API server. In a third window or terminal clone the mtapi repository and
run the server.

```
$ git clone https://gitlab.com/microtick/mtapi.git
$ cd mtapi/server
$ cp config-example.json config.json
```

Edit the config.json and set "use_database" to false.  Then run the server:

```
$ yarn install
$ node server
```

## Step 7 - Run the examples

Now you have a running local infrastructure. 

You can cd into any of the subdirectories and follow the instructions in that directory's README to run them!

