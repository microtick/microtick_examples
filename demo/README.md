## How to run the demo

Once the network is set up and running with a node, rest server and API, try out
the example in this directory:

```
$ yarn install
$ node demo_api
```

## How to fund your account

The first time through the demo, you account balance will be 0.  To fund your
account, copy the address and run the following commands:

```
$ export MTROOT=$HOME/mylocaltestnet
$ mtcli tx send bank <your account address> 10000000udai -y

