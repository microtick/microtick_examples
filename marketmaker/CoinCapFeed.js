const axios = require('axios')
const WebSocketClient = require('websocket').w3cwebsocket

const DataFeed = require('./DataFeed')

const lookup = {
  XBTUSD: "bitcoin",
  ETHUSD: "ethereum"
}

const coincap = "http://api.coincap.io/v2"

class CoinCapFeed extends DataFeed {
  
  constructor(symbol) {
    super("CoinCap:" + symbol)
    this.symbol = symbol
    this.sym = lookup[symbol]
    this.interval = 60 
    this.datalen = 360
    this.msgCount = 0
    this.lastMessage = Date.now()
  }

  async init() {
    var started = false
    do {
      try {
        let query = coincap + "/assets/" + this.sym
        const res = await axios.get(query)
        //this.latest = parseFloat(res.data.data.priceUsd)

        const now = Date.now()
        const start = now - this.datalen * 60000
        query = coincap + "/assets/" + this.sym + "/history?interval=m1&start=" + start + "&end=" + now
        const hist = await axios.get(query)

        const data = hist.data.data.map(d => {
          return parseFloat(d.priceUsd)
        })
        this.data = data.slice(-this.datalen)
        this.latest = this.data[this.data.length-1]
        this.notifyListeners()
        started = true
      } catch (e) {
        console.log("Error: " + this.symbol + " " + e.message)
      }
    } while (!started)
    
    this.connectServer()
    
    setInterval(async () => {
      // Fetch the latest sample
      try {
        let query = coincap + "/assets/" + this.sym
        const res = await axios.get(query)
        var latest = parseFloat(res.data.data.priceUsd)
        this.data.push(latest)
        this.data = this.data.slice(-this.datalen)
        this.notifyListeners()
      } catch (e) {
        console.log(e.message)
      }
      
      // Check that we're receving blocks
      if (Date.now() - this.lastMessage > this.interval * 3000) {
        console.log("Message timeout - resetting coincap connection")
        this.pricesWs.close()
        this.connectServer()
      }
    }, this.interval * 1000)
  }
  
  connectServer() {
    this.pricesWs = new WebSocketClient('wss://ws.coincap.io/prices?assets=' + this.sym)
    this.pricesWs.onmessage = this.onMessage.bind(this)
    this.pricesWs.onclose = this.onClose.bind(this)
    this.pricesWs.onerror = this.onError.bind(this)
  }
  
  onMessage(msg) {
    const data = JSON.parse(msg.data)
    this.latest = parseFloat(data[this.sym])
  }
  
  onClose() {
    console.log("Server disconnected")
    setTimeout(() => {
      this.connectServer()
    }, 1000)
  }
  
  onError() {
    console.log("Connection error")
    this.pricesWs.close()
  }
  
  latestSpot() {
    return this.latest
  }
}

module.exports = CoinCapFeed
