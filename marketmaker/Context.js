const fs = require('fs')
const microtick = require('microtick')

class Context {
  
  constructor() {
    this.configFile = './config.json'
    this.config = JSON.parse(fs.readFileSync(this.configFile))
    this.api = new microtick(this.config.api)
  }
  
  async init() {
    // Check for account on startup
    if (this.config.account === undefined) {
    
      // Generate new account, save to config file
      await this.api.init("software", mnemonic => {
        console.log("New account mnemonic:")
        console.log(mnemonic)
      })
      const account = await this.api.getWallet()
      this.config.account = account
      fs.writeFileSync(this.configFile, JSON.stringify(this.config, null, 2))
      console.log()
      console.log("Updated your config with new generated account: ")
      console.log(this.config.account.acct)
      console.log()
      console.log("Now you need to fund it:")
      console.log("$ mtcli tx send bank " + this.config.account.acct + " 10000000000udai -y")
    
      process.exit()
    
    } else {
    
      // Start with saved account
      await this.api.init(this.config.account)
    
    }
    
    const wallet = await this.api.getWallet()
    this.account = wallet.acct
  }
    
}

module.exports = Context
