const seedrandom = require('seedrandom')
const rng = seedrandom()

class DataFeed {
  
  constructor(name) {
    this.name = name
    this.listeners = []
  }
  
  feedName() {
    return this.name
  }
  
  addListener(cb) {
    this.listeners.push(cb)
  }
  
  notifyListeners() {
    this.listeners.map(cb => {
      cb()
    })
  }

  std(interval, sampleLength) {
    var length = this.data.length
    if (sampleLength !== undefined) {
      length = Math.floor(sampleLength / this.interval)
    }
    
    // Compute log returns
    var arr = [];
    for (var i=1; i<length; i++) {
        arr.push(Math.log(1 + (this.data[i] - this.data[i-1]) / this.data[i-1]));
    }
  
    // Compute volatility
    var avg = 0;
    for (var i=0; i<arr.length; i++) {
        avg += arr[i];
    }
    avg /= arr.length;
    var std = 0;
    for (i=0; i<arr.length; i++) {
        std += Math.pow(arr[i] - avg, 2);
    }
    std = Math.sqrt(std/arr.length);
    return std * Math.sqrt(interval / this.interval);
  }
  
  latestSpot() {
    return this.data[this.data.length-1]
  }
  
  // Standard normal
  randn_bm() {
    var u = 0, v = 0
    while (u === 0) u = rng() // Convert [0,1) to (0,1)
    while (v === 0) v = rng()
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v )
  }
  
}

module.exports = DataFeed
