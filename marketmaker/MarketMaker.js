const BN = require('bignumber.js')

class MarketMaker {
    
  constructor(ctx, sym) {
    this.symbol = sym
    this.ctx = ctx
    this.feeds = []
    
    this.premiums = {}
    
    this.backing = {}
    this.openinterest = {}
    this.opencallinterest = {}
    this.openputinterest = {}
    this.quotes = {}
    this.trades = {}
    
    this.ctx.api.addBlockHandler(this.blockHandler.bind(this))
    
    const info = this.ctx.api.getAccountInfo(this.ctx.account)
    this.lastBalance = new BN(info.balance).plus(info.quoteBacking).plus(info.tradeBacking).plus(info.settleBacking)
    
    this.ctx.api.addAccountHandler((key, data) => {
      if (key === "trade.short") {
        console.log("Trade: " + JSON.stringify(data, null, 2))
      }
      if (key === "settle.short") {
        console.log("Trade settled: " + JSON.stringify(data, null, 2))
      }
    })
  }
  
  async addFeed(feed) {
    this.feed = feed
    this.feed.addListener(this. onFeedUpdate.bind(this))
    await this.feed.init()
  }
  
  async blockHandler(block) {
    var blockInfo = ""
    const blocklog = msg => {
      if (blockInfo.length > 0) blockInfo += "\n"
      blockInfo += msg
    }
    var updateInfo = ""
    const updatelog = msg => {
      if (updateInfo.length > 0) updateInfo += "\n"
      updateInfo += msg
    }
    
    blocklog("Block " + block.height) 
    this.backing = {}
    this.openinterest = {}
    this.opencallinterest = {}
    this.openputinterest = {}
    this.quotes = {}
    this.trades = {}
    for (var i=0; i<this.ctx.config.state.durs.length; i++) {
      const dur = this.ctx.config.state.durs[i]
      this.backing[dur.name] = new BN(0) 
      this.openinterest[dur.name] = new BN(0)
      this.opencallinterest[dur.name] = new BN(0)
      this.openputinterest[dur.name] = new BN(0)
      this.quotes[dur.name] = []
      this.trades[dur.name] = []
    }
    
    const info = await this.ctx.api.getAccountInfo(this.ctx.account)
    var acctValue = new BN(info.balance).plus(info.quoteBacking).plus(info.tradeBacking).plus(info.settleBacking)
    blocklog("  Account Value: " + acctValue.toFixed(6))
    
    // Collect active quotes statistics for this block
    blocklog("  Active quotes: " + info.activeQuotes)
    for (var i=0; i<info.activeQuotes.length; i++) {
      const id = info.activeQuotes[i]
      const quote = await this.ctx.api.getLiveQuote(id)
      this.quotes[quote.duration].push(quote)
      this.backing[quote.duration] = this.backing[quote.duration].plus(quote.backing)
    }
    
    // Collect active trades statistics for this block
    blocklog("  Active trades: " + info.activeTrades)
    for (i=0; i<info.activeTrades.length; i++) {
      const id = info.activeTrades[i]
      const trade = await this.ctx.api.getLiveTrade(id)
      this.trades[trade.duration].push(trade)
      const cpbacking = trade.counterparties.reduce((acc, cp) => {
        if (cp.short === this.ctx.account) {
          acc = acc.plus(cp.backing)
        }
        return acc
      }, new BN(0))
      this.openinterest[trade.duration] = this.openinterest[trade.duration].plus(cpbacking)
      if (trade.option === "call") {
        this.opencallinterest[trade.duration] = this.opencallinterest[trade.duration].plus(cpbacking)
      }
      if (trade.option === "put") {
        this.openputinterest[trade.duration] = this.openputinterest[trade.duration].plus(cpbacking)
      }
      
      // If this trade has expired, let's settle it and collect the settle incentive!
      if (block.time >= trade.expiration) {
        updatelog("Settling trade: " + trade.id)
        this.ctx.api.settleTrade(trade.id)
      }
    }
    
    // Get the current Microtick consensus price
    try {
      var spot = await this.ctx.api.getMarketSpot(this.symbol)
      this.microtickSpot = spot.consensus
    } catch (err) {
      this.microtickSpot = 0
    }
    
    blocklog("  External spot: " + this.externalSpot + " Microtick spot: " + this.microtickSpot)
    
    if (this.externalSpot > 0) {
      for (i=0; i<this.ctx.config.state.durs.length; i++) {
        const dur = this.ctx.config.state.durs[i]
        blocklog("  Duration: " + dur.name)
        
        const backing = this.backing[dur.name]
        const openinterest = this.openinterest[dur.name]
        const opencallinterest = this.opencallinterest[dur.name]
        const openputinterest = this.openputinterest[dur.name]
        const basepremium = this.premiums[dur.name]
        
        blocklog("    Backing=" + backing.toFixed(6) + " Open interest=" + openinterest.toFixed(6) + 
          " Call=" + opencallinterest.toFixed(6) + " Put=" + openputinterest.toFixed(6))
        
        // Mark up premium by amount set in config file
        const targetPremium = basepremium * dur.defaultMarkup
          
        blocklog("    Premium=" + new BN(basepremium).toFixed(4) + " -> " + new BN(targetPremium).toFixed(4))
        
        // create new quotes if necessary
        if (backing.isLessThan(dur.backing)) {
          const delta = new BN(dur.backing).minus(backing)
          
          updatelog("Creating quote: " + this.symbol + " " + dur.name + " " + delta + " " + 
            this.externalSpot + " " + targetPremium)
          this.ctx.api.createQuote(this.symbol, dur.name, delta.toFixed(6) + "dai", 
            this.externalSpot + "spot", targetPremium + "premium")
        }
      
        // update quotes if:
        //   quote can be modified
        // AND any of:
        //   quote is getting stale, or
        //   current target premium is less than 1/2 quote premium
        //   external spot has moved > premium / 2, or
        //   marked up call or put price is less than the current target premium
        var backingSum = new BN(0)
        for (var j=0; j<this.quotes[dur.name].length; j++) {
          const quote = this.quotes[dur.name][j]
          
          const canModify = block.time > quote.canModify
          const test1 = block.time > quote.modified + dur.seconds * 1000 * dur.staleRefresh
          const test2 = (targetPremium < quote.premium / 2) || (quote.premium < targetPremium / 2)
          const test3 = Math.abs(this.externalSpot - quote.spot) > quote.premium
          const test4 = quote.premium - Math.abs(this.microtickSpot - quote.spot) / 2 < basepremium
          if (this.updateAll || (canModify && (test1 || test2 || test3 | test4))) {
            // adjust new premium halfway between old and current
            var newPremium = quote.premium
            if (test2) newPremium = targetPremium
            var reasons = ""
            if (this.updateAll) reasons += "config "
            if (test1) reasons += "stale "
            if (test2) reasons += "premium "
            if (test3) reasons += "external "
            if (test4) reasons += "recenter"
            reasons = reasons.trim().replace(" ",",")
            updatelog("Updating quote " + quote.id + " " + dur.name + " (" + reasons + ") " + this.externalSpot + " " + newPremium)
            this.ctx.api.updateQuote(quote.id, this.externalSpot + "spot",
              newPremium + "premium")
          }
        }
      }
    }
    
    console.log(blockInfo)
    if (updateInfo.length > 0) console.log(updateInfo)
    console.log()
  }
  
  onFeedUpdate() {
    
    // This example calculates premiums for 5, 15 and 60 minute issues using only
    // the 15-minute volatility estimate from a single feed.
    // Example purposes only.
    
    const std15Minute = this.feed.std(1, 900) // 15-minute std
    this.externalSpot = this.feed.latestSpot()
    
    this.premiums = {}
    for (var i=0; i<this.ctx.config.state.durs.length; i++) {
      const dur = this.ctx.config.state.durs[i]
      
      // This example market maker uses a risk free rate of 0, because issues are all very 
      // short term time frames
      
      const obj = this.blackscholes(this.externalSpot, this.externalSpot, std15Minute, dur.seconds, 0)
      
      // Use a premium of the average of calculated call / put price
      this.premiums[dur.name] = (obj.call + obj.put) / 2
    }
    
  }
  
  // Black-Scholes pricing calculations 
  
  erf(x) {
    // save the sign of x
    var sign = (x >= 0) ? 1 : -1;
    x = Math.abs(x);
  
    // constants
    var a1 =  0.254829592;
    var a2 = -0.284496736;
    var a3 =  1.421413741;
    var a4 = -1.453152027;
    var a5 =  1.061405429;
    var p  =  0.3275911;
  
    // A&S formula 7.1.26
    var t = 1.0/(1.0 + p*x);
    var y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.exp(-x * x);
    return sign * y; // erf(-x) = -erf(x); 
  }

  cdf(x, mean, variance) {
    return 0.5 * (1 + this.erf((x - mean) / (Math.sqrt(2 * variance))))
  }
  
  ln(x) {
    return Math.log(x)
  }
  
  blackscholes(spot, strike, vol, T, r) {
    // OP = S * N(d1) - X * exp(-r * t) * N(d2)
    // d1 = (ln(S/X) + (r + v^2/2) * t) / (v * sqrt(t))
    // d2 = d1 - v * sqrt(t)
    // S = spot price
    // X = strike price
    // t = time remaining, percent of a year
    // r = risk-free interest rate, continuously compounded
    // v = annual volatility (std dev of short-term returns over 1 year)
    //      square root of the mean of the squared deviations of close-close log returns
    if (vol > 0) {
      var d1 = (this.ln(spot / strike) + (r + vol * vol / 2.0) * T) / (vol * Math.sqrt(T));
    } else {
      d1 = 0
    }
    var d2 = d1 - vol * Math.sqrt(T);
    var C = spot * this.cdf(d1, 0, 1) - strike * this.cdf(d2, 0, 1) * Math.exp(-r * T);
    var P = C - spot + strike * Math.exp(-r * T);
    return { call: C, put: P };    
  }
  
}

module.exports = MarketMaker
