This directory contains a sample market maker for running on your local testnet. Assuming you're followed the instructions
in the directory above to set up your local chain, and have a running node, REST server and API server, running the market
maker is simple:

```
$ cp config-example.json config.json
$ yarn install
$ node mm 
```

Note that this example has several issues you'll want to address and is NOT intended for production use. It's provided for
experimentation and familiarization purposes.

1. The private key is added to the config file unencrypted
2. The premium pricing works but is based on a fixed markup. In the real world short-term volatility can change.
3. There is no external hedging included, for example taking a long / short position on an external market can help
mitigate risk when being short options. Be familiar with option pricing and strategies.

