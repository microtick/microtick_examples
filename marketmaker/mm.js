const Context = require('./Context.js')
const CoinCapFeed = require('./CoinCapFeed.js')
const MarketMaker = require('./MarketMaker.js')

process.on('unhandledRejection', error => {
  if (error !== undefined) {
    console.log('unhandled promise rejection: ', error.message)
    console.log(error.stack)
  }
})

const main = async () => {
  const ctx = new Context
  await ctx.init()
  
  const market = ctx.config.market
  console.log("Using market: " + market)
  
  const accountInfo = await ctx.api.getAccountInfo(ctx.account)
  const checkBalance = accountInfo.balance
  console.log("Token balance=" + checkBalance)
  if (checkBalance < ctx.config.minAccountBalance) {
    console.log("Minimum balance=" + ctx.config.minAccountBalance)
    console.log("Deposit tokens and restart")
    process.exit(1)
  }
  console.log()
  
  const mm = new MarketMaker(ctx, market)
  await mm.addFeed(new CoinCapFeed(market), false)
}

main()
